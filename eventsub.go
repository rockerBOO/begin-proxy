package main

import (
	"gitlab.com/rockerboo/begin-proxy/eventsub"
)

func main() {
	eventsub.Serve()
}
