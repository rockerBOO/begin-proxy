# begin-proxy

A number of services to handle proxying, testing, debugging websockets.

- [Websocket Proxy](#websocket-proxy) - Simple websocket to websocket proxy.
- [Eventsub Proxy](#eventsub-proxy) - Proxy Eventsub notifications to websocket messages.
- [Websocket Server](#websocket-server) - Very simple debugging websocket server to test websocket proxy and eventsub proxy.
- [Websocket Web Debugger](#websocket-debugger) 

![Diagram](https://i.imgur.com/uFWWJ1j.png)

## Websocket Proxy


Runs a server on port 80 that proxies websockets to a backend websocket sever.
Be a server that proxies websocket messages.

### Goals

- Simple proxy to allow messages to be sent to another server (bot) without exposing the bot's IP to others.
- Secure websockets supported through nginx.

### Getting Started

```
go run proxy.go -frontend :8080 -backend ws://example.com:3000
```

then you can connect to `ws://localhost:8080` 

### Status (Alpha)

This should work but I still need to try it.

## EventSub Proxy

Take [EventSub](https://dev.twitch.tv/docs/eventsub) events and send them to websockets.

### Goals

- Make a small, secure, proxy layer for webhooks to websocket events.
- Simple for users to setup.
- Make a easy system for users to get channel events for bots.

Requires these environmental variables set. (Subject to change)

```
BEGIN_EVENTSUB_PROXY_CLIENT_ID
BEGIN_EVENTSUB_PROXY_ACCESS_TOKEN
```

### Build 

This is how im currently building it. Which probably means I will change it a few times till I get a make file in here. Pretty basic build.

```
GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build -o eventsub-proxy eventsub.go && ./eventsub-proxy
```

### Status (Unstable)

This is a work in progress. 

```
Webhook events
==============================

Client -> /subscribe
          request -> twitch eventsub

Twitch -> /verify
          response -> challenge 

Websocket proxy events
=============================

Client    -> /eventsub

onConnect -> /subscribe channel.follow 
```


## Websocket Server

Simple websocket server for testing eventsub proxy and websocket proxy.

```
go run server.go
```

## Websocket Web Client

Test websockets using this web client.

Example: Using http-server package from npm to serve web/. 

Start websocket server
```
go run server.go
```

Start http server for web/

```
npx http-server web/
```

Note: Can use any http-server you want to use.

```
http://localhost:8080/
```

