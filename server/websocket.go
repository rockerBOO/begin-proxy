package server

import (
	"log"
	"net/http"
)

func Serve() {
	hub := newHub()
	go hub.run()

	http.HandleFunc("/test", func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			log.Printf("/eventsub %v\n", err)
			return
		}

		client := &Client{hub: hub, conn: conn, send: make(chan []byte, 256)}
		hub.register <- client

		// Allow collection of memory referenced by the caller by doing all work in
		// new goroutines.
		go client.writePump()
		go client.readPump()
	})

	http.ListenAndServe(":5678", nil)
}
