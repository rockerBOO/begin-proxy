module gitlab.com/rockerboo/begin-proxy

go 1.15

require (
	github.com/go-delve/delve v1.5.1 // indirect
	github.com/gorilla/websocket v1.4.2
	github.com/koding/websocketproxy v0.0.0-20181220232114-7ed82d81a28c
)
