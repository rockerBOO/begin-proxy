package eventsub

import (
	"flag"
	"log"
	"net/http"
	"os"
)

type Message interface {
}

type WebsocketMessage struct {
	Type    string
	Message Message
}

// websocket handler /eventsub
func serveEventSub(config *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		conn, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			config.log.Printf("/eventsub %v\n", err)
			return
		}

		client := &Client{hub: config.hub, conn: conn, send: make(chan []byte, 256)}
		config.hub.register <- client

		// Allow collection of memory referenced by the caller by doing all work in
		// new goroutines.
		go client.writePump()
		go client.readPump()
	}
}

type Config struct {
	endpoint_domain     string
	eventsub_subscribed map[string]string
	hub                 *Hub
	log                 *log.Logger
}

var (
	flagLog      = flag.String("log", "", "Log file")
	flagEndpoint = flag.String("endpoint", "eventsub-proxy.rockerboo.net", "Eventsub Endpoint Domain eventsub-proxy.rockerboo.net")
)

func Serve() {
	flag.Parse()

	hub := newHub()
	go hub.run()

	var logger *log.Logger

	if len(*flagLog) > 0 {
		// Setup file logging.
		f, err := os.OpenFile(*flagLog,
			os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

		if err != nil {
			log.Println(err)
		}

		// Configure the logger
		logger = log.New(f, "[eventsub] ", log.LstdFlags|log.Lshortfile)
	}

	config := &Config{
		eventsub_subscribed: map[string]string{},
		endpoint_domain:     *flagEndpoint,
		hub:                 hub,
		log:                 logger,
	}

	// Subscribe to eventsub notifications
	http.HandleFunc("/subscribe", func(w http.ResponseWriter, r *http.Request) {
		subType := r.URL.Query().Get("type")

		subscribe(config)(subType, r)
	})

	// HTTP endpoint
	// TODO be able to customize the url
	http.HandleFunc("/handle_eventsub", eventsub_router(config))

	// Websocket Endpoint
	// TODO be able to customize the url
	http.HandleFunc("/eventsub", serveEventSub(config))

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("EventSub Proxy"))
	})

	// Start listening
	// TODO be able to change the port
	log.Println("Listen on :4567")
	http.ListenAndServe(":4567", nil)
}
