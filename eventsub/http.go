package eventsub

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

const ClientIdEnvKey = "BEGIN_EVENTSUB_PROXY_CLIENT_ID"
const AccessTokenEnvKey = "BEGIN_EVENTSUB_PROXY_ACCESS_TOKEN"

type Verification struct {
	Id        string            `json:"id"`
	Status    string            `json:"status"`
	Type      string            `json:"type"`
	Version   string            `json:"version"`
	Condition map[string]string `json:"condition"`
	Transport map[string]string `json:"transport"`
	CreatedAt string            `json:"created_at"`
}

type Subscription struct {
	Type      string            `json:"type"`
	Version   string            `json:"version"`
	Condition map[string]string `json:"condition"`
	Transport map[string]string `json:"transport"`
}

type Event struct {
	UserId              string
	UserName            string
	BroadcasterUserId   string
	BroadcasterUserName string
}

func subscribe(config *Config) func(string, *http.Request) {
	// https://dev.twitch.tv/docs/eventsub/eventsub-subscription-types
	return func(eventsub_type string, proxyReq *http.Request) {

		// Create subscription request
		subscription := Subscription{
			Type:    eventsub_type,
			Version: "1",
			Condition: map[string]string{
				"broadcaster_user_id": "1234",
			},
			Transport: map[string]string{
				"method":   "webhook",
				"callback": fmt.Sprintf("https://%s/handle_eventsub", config.endpoint_domain),
				"secret":   "s3cr37sdfkjdl",
			},
		}

		reqBody, err := json.Marshal(subscription)

		if err != nil {
			config.log.Fatal(err)
		}

		client := &http.Client{}

		buf := bytes.NewBuffer(reqBody)
		post_url := "https://api.twitch.tv/helix/eventsub/subscriptions"
		req, err := http.NewRequest("POST", post_url, buf)

		config.log.Printf("New subscription: %s\n", eventsub_type)
		config.log.Printf("Subscription payload: %v\n", buf.String())

		if err != nil {
			config.log.Fatal(err)
		}

		req.Header.Set("Client-Id", proxyReq.Header.Get("Client-Id"))
		req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", proxyReq.Header.Get("Authorization")))
		req.Header.Set("Content-Type", "application/json")

		config.log.Printf("Requesting new subscription: %s\n", eventsub_type)
		resp, err := client.Do(req)

		if err != nil {
			config.log.Fatalf("%+v", err)
		}

		// Process response from request
		// config.log.Printf("%v\n", resp)

		defer resp.Body.Close()

		if resp.StatusCode != 200 {
			type Error struct {
				Status  float64 `json:"status"`
				Error   string  `json:"error"`
				Message string  `json:"message"`
			}

			var j Error
			if err = json.NewDecoder(resp.Body).Decode(&j); err != nil {
				config.log.Printf("%v\n", err)
				return
			}

			if resp.StatusCode == 409 {
				config.log.Printf("%v %s %s\n", int(j.Status), j.Error, j.Message)
				return
			}

			config.log.Printf("%v", j)
			config.log.Printf("%d %=\n", resp.StatusCode, resp.Body)
			return
		}

		type Response struct {
			data  []Verification
			Total string
			Limit string
		}

		body := json.NewDecoder(resp.Body)
		for {
			var t Response

			if err := body.Decode(&t); err == io.EOF {
				break
			} else if err != nil {
				config.log.Fatalf("%v\n", err)
			}

			config.log.Printf("Subscription created (likely) %v\n", t)
		}
	}
}

func verify(config *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		// POST https://example.com/webhooks/callback
		// ----
		// Twitch-Eventsub-Message-Id:             e76c6bd4-55c9-4987-8304-da1588d8988b
		// Twitch-Eventsub-Message-Retry:          0
		// Twitch-Eventsub-Message-Type:           webhook_callback_verification
		// Twitch-Eventsub-Message-Signature:      sha256=f56bf6ce06a1adf46fa27831d7d15d
		// Twitch-Eventsub-Message-Timestamp:      2019-11-16T10:11:12.123Z
		// Twitch-Eventsub-Subscription-Type:      channel.follow
		// Twitch-Eventsub-Subscription-Version:   1
		// ----
		// {
		//     "challenge": "pogchamp-kappa-360noscope-vohiyo",
		//     "subscription": {
		//         "id": "f1c2a387-161a-49f9-a165-0f21d7a4e1c4",
		//         "status": "webhook_callback_verification_pending",
		//         "type": "channel.follow",
		//         "version": "1",
		//         "condition": {
		//                 "broadcaster_user_id": "12826"
		//         },
		//         "transport": {
		//             "method": "webhook",
		//             "callback": "https://example.com/webhooks/callback"
		//         },
		//         "created_at": "2019-11-16T10:11:12.123Z"
		//     }
		// }

		dec := json.NewDecoder(req.Body)

		type Verify struct {
			Challenge    string       `json:"challenge"`
			Subscription Verification `json:"subscription"`
		}

		for {
			var t Verify

			if err := dec.Decode(&t); err == io.EOF {
				break
			} else if err != nil {
				config.log.Printf("%v\n", err)
				return
			}

			config.log.Printf("%v\n", t.Challenge)
			v, _ := json.Marshal(t)
			config.log.Printf("Verify Payload %v", string(v))

			// TODO we should be validating the challenge here
			// https://dev.twitch.tv/docs/eventsub#verify-a-signature

			config.eventsub_subscribed[t.Subscription.Type] = t.Subscription.Id

			config.log.Printf("Subscribed %s %s\n", t.Subscription.Type, config.eventsub_subscribed[t.Subscription.Type])

			w.Header().Set("Content-Type", "application/json")
			w.Header().Set("Client-Id", os.Getenv(ClientIdEnvKey))
			w.Header().Set("Authorization", fmt.Sprintf("Bearer %s", os.Getenv(AccessTokenEnvKey)))

			w.WriteHeader(200)

			if _, err := w.Write([]byte(t.Challenge)); err != nil {
				config.log.Printf("%v\n", err)
				return
			}

		}
	}
}

type Notification struct {
	Subscription Subscription
	Event        Event
}

func notify(config *Config) func(*http.Request) {
	return func(req *http.Request) {
		// Twitch-Eventsub-Message-Id:             befa7b53-d79d-478f-86b9-120f112b044e
		// Twitch-Eventsub-Message-Retry:          0
		// Twitch-Eventsub-Message-Type:           notification
		// Twitch-Eventsub-Message-Signature:      sha256=d66824350041dce130e3478f5a7
		// Twitch-Eventsub-Message-Timestamp:      2019-11-16T10:11:12.123Z
		// Twitch-Eventsub-Subscription-Type:      channel.follow
		// Twitch-Eventsub-Subscription-Version:   1

		// send notification to websocket
		sub_type := req.Header.Get("Twitch-Eventsub-Subscription-Type")
		id, is_subscribed := config.eventsub_subscribed[sub_type]

		if is_subscribed {
			config.log.Printf("Not currently subscribed to: %s\n", sub_type)
			return
		}

		result, err := json.Marshal(req.Body)

		if err != nil {
			config.log.Printf("Invalid json sent for %s\n", id)
			return
		}

		config.hub.broadcast <- result
	}
}

// Route http requests
func eventsub_router(config *Config) http.HandlerFunc {
	return func(w http.ResponseWriter, req *http.Request) {
		message_type := req.Header.Get("Twitch-Eventsub-Message-Type")

		config.log.Printf("Processing message_type %s\n", message_type)

		if message_type == "webhook_callback_verification" {
			verify(config)(w, req)
			return
		}

		if message_type == "notification" {
			notify(config)(req)
			return
		}

		config.log.Printf("Unhandled message %s", message_type)
	}
}
