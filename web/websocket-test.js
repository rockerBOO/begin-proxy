
(() => {
	const $ = (selector) => document.querySelector(selector)
	const socket = new WebSocket("ws://localhost:8080/test");

	socket.onopen = function() {
		console.log("[open] Connection established");
		console.log("Sending to server");
		socket.send("My name is John");

		$("#websocket-state").innerHTML = "Open"
	};

	// socket.onmessage = function(event) {
	//   console.log(`[message] Data received from server: ${event.data}`);
	// };

	// send message from the form
	document.forms.publish.onsubmit = function() {
		let outgoingMessage = this.message.value;

		document.forms.publish.message.value = ""

		socket.send(outgoingMessage);
		return false;
	};

	// message received - show the message in div#messages
	socket.onmessage = function(event) {
		console.log("Incoming Message:" , event)
		let message = event.data;

		let messageElem = document.createElement('div');
		messageElem.textContent = message;
		document.getElementById('messages').prepend(messageElem);
	}

	socket.onclose = function(event) {
		if (event.wasClean) {
			console.log(`[close] Connection closed cleanly, code=${event.code} reason=${event.reason}`);
		} else {
			// e.g. server process killed or network down
			// event.code is usually 1006 in this case
			console.log('[close] Connection died');
		}

		$("#websocket-state").innerHTML = "Closed"
	};

	socket.onerror = function(error) {
		console.log(`[error] ${error.message}`);
	};
})()
