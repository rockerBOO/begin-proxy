package main

import (
	"flag"
	"log"
	"net/http"
	"net/url"

	"github.com/gorilla/websocket"
	"github.com/koding/websocketproxy"
)

var (
	flagBackend  = flag.String("backend", "", "Backend URL for proxying")
	flagFrontend = flag.String("frontend", ":80", "Frontend URL")
)

func main() {
	flag.Parse()
	u, err := url.Parse(*flagBackend)
	if err != nil {
		log.Fatalln(err)
	}

	upgrader := websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
		// NOTE: Bypasses origin checks and allows any host to connect
		CheckOrigin: func(r *http.Request) bool { return true },
	}

	proxy := websocketproxy.NewProxy(u)
	proxy.Upgrader = &upgrader

	err = http.ListenAndServe(*flagFrontend, proxy)

	if err != nil {
		log.Fatalln(err)
	}
}
