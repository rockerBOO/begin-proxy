#!/bin/sh

case $1 in
  run) go run $2 ;;
  build) go build $2 ;;
  test) go test ./... ;;
  lint) go vet ;;

  *) echo "Invalid task: $1"; exit 1 ;;
esac
